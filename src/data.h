#ifndef __DATA_H__
#define __DATA_H__

#include <math3d.h>

// MODELS
#include "data/cube_data.h"
#include "data/plane_data.h"
#include "data/monkey_data.h"
#include "data/dragon_data.h"

// TEXTURES
#include "data/cubemap_texture.h"
#include "data/floor_texture.h"
#include "data/dragon_texture.h"
#include "data/monkey_texture.h"

#endif
